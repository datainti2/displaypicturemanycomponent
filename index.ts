import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BasecomponentComponent } from './src/basecomponent.component';

export * from './src/basecomponent.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
   
	BasecomponentComponent
  ],
  exports: [
    
	BasecomponentComponent
  ]
})
export class DisplayPictureManyModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: DisplayPictureManyModule,
      providers: []
    };
  }
}
